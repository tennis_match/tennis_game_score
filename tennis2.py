class match:
    def __init__(self):
        self.points = {'A': 0, 'B': 0}
        self.games = {'A': 0, 'B': 0}
        self.sets = {'A': 0, 'B': 0}
        self.point_map = [0, 15, 30, 40]


    def points_to(self, player):
        if player not in 'AB':
            raise ValueError("invalid input")
            return

        self.points[player] += 1

        if self.points[player] == 4:
            self.points[player] = 0
            self.points['A' if player == 'B' else 'B'] = 0
            self.games[player] += 1

            if self.games[player] == 6:
                self.games[player] = 0
                self.games['A' if player == 'B' else 'B'] = 0
                self.sets[player] += 1
                if self.sets[player] == 1:
                    print(f"Player {player} wins the match!")
                    return

    def get_score(self):
        return {
            'points': {p: self.point_map[pt] for p, pt in self.points.items()},
            'games': self.games.copy(),
            'sets': self.sets.copy()
        }

    def print_score(self):
        score = self.get_score()
        print(f"points: A:  {score['points']['A']}, B - {score['points']['B']}")
        print(f"games: A - {score['games']['A']}, B - {score['games']['B']}")
        print(f"sets: A - {score['sets']['A']}, B - {score['sets']['B']}")


def sim_match(sequence):
    mat = match()
    for point in sequence:
        mat.points_to(point)
        mat.print_score()


input = "AABAABABABBBAAABBAAAABAABABABBBAAABBAAAABAABABABBBAAABBAA"
sim_match(input)