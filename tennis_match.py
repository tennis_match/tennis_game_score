def parse_point(point):
    if point == "15":
        return 1
    elif point == "30":
        return 2
    elif point == "40":
        return 3
    elif point == "game":
        return 4
    return 0

def check_game_winner(score1, score2):
    if score1 >= 4 and score1 >= score2 + 2:
        return 1
    elif score2 >= 4 and score2 >= score1 + 2:
        return 2
    return 0

def check_set_winner(games1, games2):
    if games1 >= 6 and games1 >= games2 + 2:
        return 1
    elif games2 >= 6 and games2 >= games1 + 2:
        return 2
    return 0

def check_match_winner(sets1, sets2):
    if sets1 >= 2:
        return 1
    elif sets2 >= 2:
        return 2
    return 0

def determine_winner(input_string):
    points = input_string.split()
    score1 = score2 = 0
    games1 = games2 = 0
    sets1 = sets2 = 0

    for point in points:
        if point == "Player1":
            score1 += 1
        elif point == "Player2":
            score2 += 1

        game_winner = check_game_winner(score1, score2)
        if game_winner == 1:
            games1 += 1
            score1 = score2 = 0
        elif game_winner == 2:
            games2 += 1
            score1 = score2 = 0

        set_winner = check_set_winner(games1, games2)
        if set_winner == 1:
            sets1 += 1
            games1 = games2 = 0
        elif set_winner == 2:
            sets2 += 1
            games1 = games2 = 0

        match_winner = check_match_winner(sets1, sets2)
        if match_winner == 1:
            return "Player1 wins the match"
        elif match_winner == 2:
            return "Player2 wins the match"

    return "Match not finished"

input_string = "Player1 Player2 Player1 Player1 Player1 Player2 Player2 Player2 Player1 Player2 Player2 Player2 Player1 Player1 Player1"
print(determine_winner(input_string))

